package eztv

import "testing"

func TestGet(t *testing.T) {
	id := "tt8111088" // The Mandalorian
	items, err := Get(id)
	if err != nil {
		t.Fatal(err)
	}
	if len(items) != 100 {
		t.Fatal("did not get 100 results")
	}
}
