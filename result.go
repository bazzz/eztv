package eztv

type result struct {
	ID    string `json:"imdb_id"`
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Items []Item `json:"torrents"`
}
