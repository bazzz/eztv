package eztv

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const baseURL = "https://eztv.re/api/get-torrents"

// Get returns the latest max 100 episodes for a provided IMDB id.
func Get(id string) ([]Item, error) {
	if !strings.HasPrefix(id, "tt") {
		return nil, errors.New("parameter id must start with string tt")
	}
	url := baseURL + "?limit=100&imdb_id=" + id[2:]
	data, err := call(url)
	if err != nil {
		return nil, fmt.Errorf("could not get data for %v: %w", id, err)
	}
	result := result{}
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, fmt.Errorf("could not unmarshal json for %v: %w", id, err)
	}
	return result.Items, nil
}

func call(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	if response.StatusCode != 200 {
		return nil, errors.New("got http status response: " + response.Status)
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return data, nil
}
