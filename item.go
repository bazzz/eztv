package eztv

type Item struct {
	Hash             string `json:"hash"`
	Title            string `json:"title"`
	Season           string `json:"season"`
	Episode          string `json:"episode"`
	Seeds            int    `json:"seeds"`
	Peers            int    `json:"peers"`
	DateReleasedUnix int64  `json:"date_released_unix"`
	SizeBytes        string `json:"size_bytes"`
}
